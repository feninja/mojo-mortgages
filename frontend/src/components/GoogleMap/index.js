import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    width: 40%;
`;

const GoogleMap = () => {
    return (
        <Container>
            <iframe
                title="Map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d75993.25869265817!2d-2.293502315095846!3d53.47222497481179!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487a4d4c5226f5db%3A0xd9be143804fe6baa!2sManchester!5e0!3m2!1sen!2suk!4v1598562611036!5m2!1sen!2suk"
                width="100%"
                height="100%"
                frameBorder="0"
                style={{ border: 0 }}
                allowFullScreen=""
                aria-hidden="false"
                tabIndex="0"
            ></iframe>
        </Container>
    );
};

export default GoogleMap;
