import React from 'react';
import { Route, Switch } from 'react-router-dom';
import styled from 'styled-components';

import {
    BuildingsContext,
    BuildingsProvider,
} from '../../contexts/BuildingsContext.js';

import LeftPanel from '../LeftPanel';

import Buildings from '../Buildings';
import ContactUs from '../ContactUs';

const StyledWrapper = styled.div`
    width: 100%;
    display: flex;
    height: 100%;
`;

const Main = styled.div`
    width: 80%;
    overflow: scroll;
`;

const Wrapper = () => {
    return (
        <StyledWrapper>
            <LeftPanel />
            <Main>
                <Switch>
                    <Route path="/contact" component={ContactUs} />
                    <BuildingsProvider>
                        <BuildingsContext.Consumer>
                            {({ buildings, fetchBuildings }) => (
                                <Route exact path="/">
                                    <Buildings
                                        buildings={buildings}
                                        fetchBuildings={fetchBuildings}
                                    />
                                </Route>
                            )}
                        </BuildingsContext.Consumer>
                    </BuildingsProvider>
                </Switch>
            </Main>
        </StyledWrapper>
    );
};

export default Wrapper;
