import React from 'react';
import { NavLink } from 'react-router-dom';

import styled from 'styled-components';

const NavBlock = styled.div`
    width: 100%;
    margin-top: 52px;
`;

const Li = styled.li`
    a {
        background-color: transparent;
        padding: 9px 16px;
        display: block;
        transition: 0.3s background-color ease-in-out;
    }
    a.active {
        background-color: #1665d8;
        transition: 0.3s background-color ease-in-out;
    }
`;

const NavHeading = styled.span`
    color: #9ea0a5;
    font-size; 12px;
    padding: 9px 16px;
    display: block;
    text-transform: uppercase;
`;

const Navigation = () => {
    return (
        <React.Fragment>
            <NavBlock>
                <nav>
                    <ul>
                        <NavHeading>Menu</NavHeading>
                        <Li>
                            <NavLink to="/dashboard">Dashboard</NavLink>
                        </Li>
                        <Li>
                            <NavLink exact to="/">
                                Buildings
                            </NavLink>
                        </Li>
                    </ul>
                </nav>
            </NavBlock>
            <NavBlock>
                <nav>
                    <ul>
                        <NavHeading>Support</NavHeading>
                        <Li>
                            <NavLink to="/contact">Contact Us</NavLink>
                        </Li>
                    </ul>
                </nav>
            </NavBlock>
        </React.Fragment>
    );
};

export default Navigation;
