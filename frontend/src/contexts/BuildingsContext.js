import React from 'react';

import { fetchBuildings } from '../services/fetchBuildings';

export const BuildingsContext = React.createContext();

export class BuildingsProvider extends React.Component {
    constructor() {
        super();
        this.state = {
            buildings: {},
            buildingsLoaded: false,
            displayRow: false,
            search: '',
        };
    }

    fetchBuildings = () => {
        fetchBuildings().then((data) => {
            const list = Object.keys(data).map((key) => data[key]);

            this.setState({
                defaultBuildings: list,
                buildings: list,
                buildingsLoaded: true,
            });
        });
    };

    changeLayout = (val) => {
        val !== this.state.displayRow &&
            this.setState({
                displayRow: !this.state.displayRow,
            });
    };

    filterListingType = (e) => {
        const selected = e.target.value;

        if (selected === 'all') {
            console.log('here');
            this.setState({
                buildings: this.state.defaultBuildings,
            });
        }

        if (selected === 'sale') {
            const list = this.state.defaultBuildings.filter(
                (item) => item.listingType === 'For Sale'
            );

            this.setState({
                buildings: list,
            });
        }

        if (selected === 'rent') {
            const list = this.state.defaultBuildings.filter(
                (item) => item.listingType === 'For Rent'
            );

            this.setState({
                buildings: list,
            });
        }
    };

    searchListingName = (e) => {
        const val = e.target.value;

        this.setState({
            search: val,
        });
    };

    render() {
        return (
            <BuildingsContext.Provider
                value={{
                    ...this.state,
                    fetchBuildings: this.fetchBuildings,
                    changeLayout: this.changeLayout,
                    filterListingType: this.filterListingType,
                    searchListingName: this.searchListingName,
                }}
            >
                {this.props.children}
            </BuildingsContext.Provider>
        );
    }
}
