import React from 'react';

import styled from 'styled-components';

import Wrapper from '../Wrapper';

const Container = styled.div`
    margin: 0 auto;
    width: 100%;
    height: 100%;
`;

class App extends React.Component {
    render() {
        return (
            <Container>
                <Wrapper />
            </Container>
        );
    }
}

export default App;
