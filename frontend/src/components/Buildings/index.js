import React from 'react';
import styled from 'styled-components';

import { BuildingsContext } from '../../contexts/BuildingsContext.js';

import BuildingList from './BuildingList';
import TopBar from './TopBar';
import GoogleMap from '../GoogleMap';

const Section = styled.div`
    width: 100%;
    display: flex;
`;

class Buildings extends React.Component {
    componentDidMount() {
        this.props.fetchBuildings();
    }

    render() {
        return (
            <React.Fragment>
                <TopBar />
                <Section>
                    <BuildingsContext.Consumer>
                        {({
                            buildings,
                            buildingsLoaded,
                            displayRow,
                            changeLayout,
                            search,
                        }) => (
                            <BuildingList
                                buildings={buildings}
                                buildingsLoaded={buildingsLoaded}
                                displayRow={displayRow}
                                changeLayout={changeLayout}
                                search={search}
                            />
                        )}
                    </BuildingsContext.Consumer>
                    <GoogleMap />
                </Section>
            </React.Fragment>
        );
    }
}

export default Buildings;
