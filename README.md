# README #

### Instructions for use ###

* ``` cd api-server ```
** ```npm i && npm start ```
* ``` cd frontend ```
** ``` npm i && npm start ```

### Missing features / incomplete tasks ###

* Top left page title
* language select dropdown
* images served from API
* Select styling
* Google Map API
* Icons for All Buildings Select
* Code refactoring
* Responsive styling
* Browser Testing
* Device Testing