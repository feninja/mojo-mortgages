import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const StyledForm = styled.form`
    position: relative;
`;

const StyledInput = styled.input`
    border: 1px solid #e2e5ed;
    height: 38px;
    padding: 0 16px;
`;

const Icon = styled.div`
    position: absolute;
    right: 12px;
    top: 50%;
    margin-top: -8px;
    color: #9ea0a5;
`;

const Search = ({ searchListingName }) => {
    return (
        <StyledForm>
            <StyledInput
                onChange={searchListingName}
                type="text"
                placeholder="Type to search..."
            />
            <Icon>
                <FontAwesomeIcon icon={faSearch} />
            </Icon>
        </StyledForm>
    );
};

export default Search;
