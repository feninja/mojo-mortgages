import React from 'react';
import styled from 'styled-components';

import { BuildingsContext } from '../../../contexts/BuildingsContext.js';

import Select from './Select';
import Search from './Search';

const Container = styled.div`
    padding: 2%;
    display: flex;
    justify-content: space-between;
`;

const TopBar = () => {
    return (
        <Container>
            <BuildingsContext.Consumer>
                {({ filterListingType }) => (
                    <Select filterListingType={filterListingType} />
                )}
            </BuildingsContext.Consumer>
            <BuildingsContext.Consumer>
                {({ searchListingName }) => (
                    <Search searchListingName={searchListingName} />
                )}
            </BuildingsContext.Consumer>
        </Container>
    );
};

export default TopBar;
