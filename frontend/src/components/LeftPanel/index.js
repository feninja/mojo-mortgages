import React from 'react';
import styled from 'styled-components';
import Navigation from '../Navigation';

const StyledPanel = styled.div`
    width: 20%;
    background-color: #252529;
    padding: 14px;
`;

const LeftPanel = () => {
    return (
        <StyledPanel>
            <Navigation />
        </StyledPanel>
    );
};

export default LeftPanel;
