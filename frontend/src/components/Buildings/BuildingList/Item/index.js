import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faBriefcase } from '@fortawesome/free-solid-svg-icons';

import Shard from '../../../../images/shard.jpg';

const StyledItem = styled.div`
    width: ${(props) => (props.fullwidth ? '100%' : '48%')};
    color: #9ea0a5;
    margin-bottom: 32px;
`;

const Image = styled.div`
    background: url(${Shard}) no-repeat center / cover;
    width: 100%;
    height: 200px;
`;

const Name = styled.div`
    color: #3e3f42;
    font-size: 16px;
    margin-top: 20px;
    margin-bottom: 6px;
`;

const AddressItem = styled.p`
    margin: 0;
    width: 60%;
    line-height: 22px;
`;

const InfoRow = styled.div`
    width: 100%;
    display: flex;
    border-bottom: 1px solid #eaedf3;
    padding: 18px 0;
    span {
        margin-left: 24px;
        &:first-child {
            margin-left: 0;
        }
    }
`;
const Icon = styled.span`
    margin-right: 6px;
`;

const BottomRow = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-top: 18px;
`;

const Price = styled.p`
    margin: 0;
    color: #3e3f42;

    font-size: 18px;
`;
const ListingType = styled.p`
    margin: 0;
    color: #1665d8;
    font-size: 18px;
`;

const Item = ({
    name,
    address,
    users,
    offices,
    price,
    listingType,
    displayRow,
}) => {
    return (
        <StyledItem fullwidth={displayRow}>
            <Image />
            <Name>{name}</Name>

            <AddressItem>
                {address.addressNumber} {address.addressLine1},{' '}
                {address.addressLine2}, {address.postcode}
            </AddressItem>

            <InfoRow>
                <span>
                    <Icon>
                        <FontAwesomeIcon icon={faUser} /> {users} users
                    </Icon>
                </span>
                <span>
                    <Icon>
                        <FontAwesomeIcon icon={faBriefcase} />
                    </Icon>
                    {offices} offices
                </span>
            </InfoRow>
            <BottomRow>
                <ListingType>{listingType}</ListingType>
                <Price>{price}</Price>
            </BottomRow>
        </StyledItem>
    );
};

export default Item;
