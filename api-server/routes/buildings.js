const buildingRoutes = (app, fs) => {
    // variables
    const dataPath = './data/buildings.json';

    // READ
    app.get('/buildings', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }

            res.send(JSON.parse(data));
        });
    });
};

module.exports = buildingRoutes;
