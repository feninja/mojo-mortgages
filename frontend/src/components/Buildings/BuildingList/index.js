import React from 'react';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faColumns } from '@fortawesome/free-solid-svg-icons';

import Item from './Item';

const TopRow = styled.div`
    width: 100%;
    margin: 28px 0;
    display: flex;
    justify-content: space-between;
`;

const DisplayRow = styled.div`
    display: flex;
    font-size: 16px;
`;

const DisplayItem = styled.div`
    margin-left: 12px;
    cursor: pointer;
    color: ${(props) => `${props.active}`};
    &:first-child {
        margin-left: 0;
    }
`;

const Title = styled.h2`
    font-size: 18px;
    color: #3e3f42;
    margin: 0;
`;

const Grid = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    flex-flow: row wrap;
`;

const Container = styled.div`
    width: 60%;
    padding: 0 2%;
    border: 1px solid #eaedf3;
`;

const Error = styled.p`
    font-size: 18px;
    margin-top: 24px;
`;

const BuildingList = ({
    buildings,
    buildingsLoaded,
    displayRow,
    changeLayout,
    search,
}) => {
    let list = search
        ? buildings.filter((item) =>
              item.name.toLowerCase().includes(search.toLowerCase())
          )
        : buildings;

    return (
        <Container>
            {buildingsLoaded && (
                <React.Fragment>
                    <TopRow>
                        <Title>{list.length} Buildings</Title>
                        <DisplayRow>
                            <DisplayItem
                                onClick={() => changeLayout(true)}
                                active={displayRow ? '#1665d8' : '#9ea0a5'}
                            >
                                <FontAwesomeIcon icon={faList} />
                            </DisplayItem>
                            <DisplayItem
                                onClick={() => changeLayout(false)}
                                active={!displayRow ? '#1665d8' : '#9ea0a5'}
                            >
                                <FontAwesomeIcon icon={faColumns} />
                            </DisplayItem>
                        </DisplayRow>
                    </TopRow>
                    {list.length > 0 ? (
                        <Grid>
                            {list.map(
                                ({
                                    id,
                                    name,
                                    address,
                                    listingType,
                                    users,
                                    offices,
                                    price,
                                }) => {
                                    return (
                                        <Item
                                            key={id}
                                            name={name}
                                            address={address}
                                            listingType={listingType}
                                            users={users}
                                            offices={offices}
                                            price={price}
                                            displayRow={displayRow}
                                        />
                                    );
                                }
                            )}
                        </Grid>
                    ) : (
                        <Error>
                            No results matching your criteria please refine your
                            search
                        </Error>
                    )}
                </React.Fragment>
            )}
        </Container>
    );
};

export default BuildingList;
