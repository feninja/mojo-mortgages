import React from 'react';

const Select = ({ filterListingType }) => {
    return (
        <select onChange={filterListingType}>
            <option value="all">All Buildings</option>
            <option value="sale">For Sale</option>
            <option value="rent">For Rent</option>
        </select>
    );
};

export default Select;
